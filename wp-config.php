<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'e5pblog_wp');

/** MySQL database username */
define('DB_USER', 'e5pblog_wp');

/** MySQL database password */
define('DB_PASSWORD', '@K5UrCtQ');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*nSbK_G+6|B4WZkHI,Wvl$AMPj},8U?q/V!K|*pZK#~mpx&dYTP8GLijA~^ncv}|');
define('SECURE_AUTH_KEY',  'sKb~T/md4i>_TGe7<~:ou]0(h~I|wM{+<[?Je4+$+V`-P1*lwV&!miVKFdTc}71|');
define('LOGGED_IN_KEY',    '~/LmJA^<23OBo,YKt!`A +2PCB%w&LYRS&vTcI#,{].}YC#J4Uk#dqjt9D||4G +');
define('NONCE_KEY',        '*u4a4|`>c++A|!l_+iNFg9znJ?^UC[8+-Njw+,uKrAZA[@_#e69;w/<4i+g=1>d=');
define('AUTH_SALT',        'Lr|vWI1)z|l[.sN,-7}68yD5ahQ~q]G*u~MLJY|x.6#+pVI)s@_dI;U-y$@_Sd+0');
define('SECURE_AUTH_SALT', '2@R0Mn#,]vGMv47W;^-R7Rg@$,c6j!8fF-*pr/fY[#pO`>i}:r`V~-@B7w;A{mPr');
define('LOGGED_IN_SALT',   'Jo{=b<>!pGD)?2;Tax)}pP][qMk:1|EU<t[nEC-cJ$.p*6^+b2n)UHQ,6 f4XWtT');
define('NONCE_SALT',       'ziH[@|RN#s`)eF{VAVym)$sD/xz[+SN$WzD6{Q:mLsLtw8x(Q20u-FK.TS&J6|,|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
