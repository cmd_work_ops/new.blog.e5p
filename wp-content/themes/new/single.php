<?php
/**
 * The Template for displaying all single posts.
 *
 * @package aThemes
 */

get_header();
?>

	<div id="primary" class="content-area single-page-create single-modify">
	<div style="padding: 0 1% 0 2%;">
		<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>

		<div class="entry-meta">
			<?php athemes_posted_on(); ?>

			<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
			<span class="comments-link"> <?php comments_popup_link( __( 'Leave a Comment', 'athemes' ), __( '1 Comment', 'athemes' ), __( '% Comments', 'athemes' ) ); ?></span>
			<?php endif; ?>
		<!-- .entry-meta --></div>
	<!-- .entry-header --></header></div>
	
		<div id="content" class="site-content" role="main">
		<h3><?php //the_author(); ?></h3>
		<?php while ( have_posts() ) : the_post(); ?>
		
		
			<?php get_template_part( 'content', 'single' ); ?>

			<?php if ( get_theme_mod( 'author_bio' ) != 1 ) : ?>
			<!--	<div class="clearfix author-info">
					<div class="author-photo "><?php echo get_avatar( get_the_author_meta( 'ID' ) , 75 ); ?></div>
					<div class="author-content">
						<h3><?php the_author(); ?></h3>
						<p><?php the_author_meta( 'description' ); ?></p>
						<div class="author-links">
							<a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="me">More Posts</a>

							<?php if ( get_the_author_meta( 'twitter' ) ) : ?>
							<a href="http://twitter.com/<?php echo get_the_author_meta( 'twitter' ); ?>">Twitter</a>
							<?php endif; ?>

							<?php if ( get_the_author_meta( 'facebook' ) ) : ?>
							<a href="https://facebook.com/<?php echo get_the_author_meta( 'facebook' ); ?>">Facebook</a>
							<?php endif; ?>

							<?php if ( get_the_author_meta( 'linkedin' ) ) : ?>
							<a href="http://linkedin.com/in/<?php echo get_the_author_meta( 'linkedin' ); ?>">LinkedIn</a>
							<?php endif; ?>
						</div>
					</div>
				<!-- .author-info </div>  --> 
			<?php endif; ?>

			<?php athemes_content_nav( 'nav-below' ); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() )
					comments_template();
			?>

		<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>