<?php
/**
 * Template Name: Home Template
 *
 * @package SimpleMag
 * @since 	SimpleMag 1.1
 * */
?>

<?php get_header(); ?>
<section id="content" role="main" class="clearfix animated">
    <div class="wrapper overflow">
        <?php
        global $ti_option;
        //**************  Code to get latest post from History , Science Blog   
        $original_blog_id = get_current_blog_id(); // get current blog
        //********** History = 6 , Science = 7
        $bids = array(6, 7); // all the blog_id's to loop through EDIT
        foreach ($bids as $bid):
            switch_to_blog($bid); //switched to blog with blog_id $bid
            // ... your code for each blog ...
            $blog_details = get_blog_details($bid);
            $latest_posts = get_posts('&orderby=post_date&showposts=12&order=DESC');
            ?>
            <section class="home-section category-posts">
                <header class="section-header">
					<h2 class="title"><span>OUR BRANDS</span></h2>
					<div>
						<a href='http://madefrom.com/history'><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/history-small.gif' alt='History' title='History'></a>
						<a href='http://madefrom.com/science'><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/science-small.gif' alt='Science' src='Science'></a>
						<a href='http://madefrom.com/media'><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/media-small.gif' alt='Media' src='Media'></a>
					</div>
					<h2 class="title"><span><?php if ($bid == 6) echo "HISTORY"; elseif ($bid == 7) echo "SCIENCE"; ?></span></h2>
				</header>
                <div class="grids entries">        
                    <section class="per-category"> 
                        <?php foreach ($latest_posts as $post) : setup_postdata($post); ?>       

                            <article <?php post_class("grid-4"); ?>>
                                <figure class="entry-image">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            the_post_thumbnail('medium');
                                        } elseif (first_post_image()) { // Set the first image from the editor
                                            echo '<img src="' . first_post_image() . '" class="wp-post-image" />';
                                        }
                                        ?>
                                    </a>
                                </figure>
                                <header class="entry-header">
                                    <!--<div class="entry-meta">
                                       <span class="entry-category"><?php the_category(', '); ?></span>
                                       <span class="entry-date"><?php the_time(get_option('date_format')); ?></span>
                                    </div>-->
                                    <h2 class="entry-title">
                                        <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                    </h2>
                                    <!--<?php if ($ti_option['site_author_name'] == 1) { ?>
                                                                    <span class="entry-author">
                                        <?php _e('By', 'themetext'); ?> <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author"><?php the_author_meta('display_name'); ?></a>
                                                                    </span>
                                    <?php } ?>-->
                                </header>
                                <?php if (get_sub_field('category_excerpt') == 'enable') { ?>
                                    <div class="entry-summary">
                                        <?php the_excerpt(); ?>
                                    </div>
                                <?php } ?>

                            </article>                          
                        <?php endforeach; ?>
                    </section> 
                </div>
            </section>
            <?php
        endforeach;
        switch_to_blog($original_blog_id); //switched back to current blog
        ?>
    </div>
</section>

<?php get_footer(); ?>

